Install requirements:
 `pip install -r requirements.txt`
 
In nosql_db_enumeration.py update the fields to your requirements:
 - host
 - url 
 - datafield
 
Set tables variable with the tables to scan on line 17 to skip enumerating the
table names.